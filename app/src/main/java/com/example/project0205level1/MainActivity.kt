package com.example.project0205level1

fun main() {
    val firstName: String = "Lara"
    val lastName: String = "Cooper"
    var height: Int = 166
    val weight: Float = 45.6F
    var isChild: Boolean = weight <= 40 || height <= 150

    var info: String = """
            Name - $firstName
            Surname - $lastName
            Height - $height 
            Weight - $weight
            Are you child - $isChild
            """

    println("Вывод информации 1: $info")

    height = 140
    isChild = weight <= 40 || height <= 150

    var info2: String = """
            Name - $firstName
            Surname - $lastName
            Height - $height 
            Weight - $weight
            Are you child - $isChild
            """

    println("Вывод информации 2: $info2")
}